﻿using Map.Models;
using Microsoft.AspNetCore.Mvc;

namespace Map.Service.Controllers
{
    [Route("/")]
    public class DefaultController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Json
            (
                new List<Names>
                {
                    new Names
                    {
                        Ru = "name-ru-1",
                        Kz = "name-kz-1",
                        En = "name-en-1"
                    },
                    new Names
                    {
                        Ru = "name-ru-2",
                        Kz = "name-kz-2",
                        En = "name-en-2"
                    },
                    new Names
                    {
                        Ru = "name-ru-3",
                        Kz = "name-kz-3",
                        En = "name-en-3"
                    }
                }
            );          
        }
    }
}
